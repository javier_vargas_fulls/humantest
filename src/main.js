import Vue from 'vue';
import App from './App.vue';
import Meta from 'vue-meta';
Vue.prototype.$eventHub = new Vue(); 

Vue.use(Meta);

new Vue({
  el: '#app',
  render: h => h(App)
})
