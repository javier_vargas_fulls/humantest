export default function fetchData () {
    const request = new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET',  'json/data.json');
        xhr.onload = () => resolve(JSON.parse(xhr.response));
        xhr.send();
    })

    return request;
}